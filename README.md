# Sudoka

This is a Sudoku application that delivers an experience as close as possible
to solving Sudokus with pen and paper,
but with the benefits of a digital solution:

- taking notes and fixing errors doesn't cause a mess on the board
- you spend more time thinking and less time writing
  once you have internalized the keybindings to take notes and insert numbers -
  it's *much* faster than writing by hand
- a machine makes no mistakes when checking the board for errors
- a machine can generate a practically infinite number of Sudokus for free

## Features

The feature set is designed to include everything you'd expect
from a Sudoku application, but no more than that:

- five difficulty levels
- taking notes in cells to remember which numbers might go in that cell
- checking the board for errors
  (only on demand, not every time the board changes)
- restarting games (re-doing the same board again)
  when you have no idea how to fix a mistake
- distraction-free UI

## Keymap

The keymap is built to keep the hands on or close to the home row
as much as possible while maintaining intuitive mappings:

- Navigating the Sudoku board using the Vim navigation keys `h`, `j`, `k`, `l`
  - holding `shift` does larger jumps
- Taking notes in the focused cell works using the keys `1` - `9`
  or a virtual number block on the left hand
  - press to add note, press again to remove
  - the virtual number block means `w`, `e`, `r`; `s`, `d`, `f`; `x`, `c`, `v`;
    can alternatively be used for the numbers 1 - 9 in ascending order
- Numbers are inserted in the focused cell the same way notes are taken,
  but you hold `shift` while pressing the key
- Other actions you might use frequently are accessible via menu items
  that have `CMD-...` keybindings:
  - `CMD-c` to **c**heck the board for errors
  - `CMD-r` to **r**estart the game you are currently playing
  - `CMD-n` to start a **n**ew game
- Actions that aren't used often (normally) are accessible via menu items only:
  - Changing the difficulty of new boards
