package de.lorchtner.sudoker.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * This is thread safe, even though that is not required anymore.
 */
public abstract class EventBus<E, L extends EventListener<E>> {
    private static final Logger logger = LoggerFactory.getLogger(EventBus.class);

    private final ArrayList<WeakReference<L>> subscribers = new ArrayList<>();
    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    public void subscribe(L listener) {
        Objects.requireNonNull(
                listener,
                "Null cannot subscribe to events"
        );

        final var reference = new WeakReference<>(listener);
        try {
            lock.writeLock().lock();
            subscribers.add(reference);
        } finally {
            lock.writeLock().unlock();
        }

        logger.debug("Added event bus listener {}.", listener);
    }

    public void unsubscribe(L listener) {
        Objects.requireNonNull(
                listener,
                "Null cannot be unsubscribed from events"
        );

        try {
            lock.writeLock().lock();

            final var iterator = subscribers.iterator();

            while (iterator.hasNext()) {
                final var next = iterator.next().get();

                if (next == null) {
                    // prune dropped refs
                    iterator.remove();
                } else if (next == listener) {
                    // this is the element to search for
                    iterator.remove();
                    break;
                }
            }
        } finally {
            lock.writeLock().unlock();
        }

        logger.debug("Removed event bus listener {}.", listener);
    }

    public void publish(E event) {
        Objects.requireNonNull(event, "Cannot publish null events");

        try {
            lock.readLock().lock();

            if (subscribers.isEmpty()) {
                return;
            }

            for (final var subscriberRef : subscribers) {
                final var subscriber = subscriberRef.get();

                if (subscriber != null) {
                    subscriber.onEvent(event);
                }
            }
        } finally {
            lock.readLock().unlock();
        }

        logger.trace("Published event {}.", event);
    }
}
