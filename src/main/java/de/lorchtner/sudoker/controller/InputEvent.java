package de.lorchtner.sudoker.controller;

import de.lorchtner.sudoker.model.Difficulty;
import de.lorchtner.sudoker.view.Direction;

public sealed interface InputEvent {
    StartNewGame START_NEW_GAME = new StartNewGame();
    RestartGame RESTART_GAME = new RestartGame();
    CheckBoard CHECK_BOARD = new CheckBoard();

    record MoveFocus(Direction direction, int cellDistance) implements InputEvent {}
    record ToggleValue(byte value) implements InputEvent {}
    record ToggleNote(byte value) implements InputEvent {}
    record SelectDifficulty(Difficulty difficulty) implements InputEvent {}

    final class StartNewGame implements InputEvent {
        private StartNewGame() {}
    }
    final class RestartGame implements InputEvent {
        private RestartGame() {}
    }
    final class CheckBoard implements InputEvent {
        private CheckBoard() {}
    }
}
