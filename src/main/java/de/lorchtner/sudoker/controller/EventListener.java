package de.lorchtner.sudoker.controller;

public interface EventListener<Event> {
    /**
     * @param event Guaranteed to be non-null
     */
    void onEvent(Event event);
}
