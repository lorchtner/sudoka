package de.lorchtner.sudoker.controller;

import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatLightLaf;
import com.jthemedetecor.OsThemeDetector;
import de.lorchtner.sudoker.model.Difficulty;
import de.lorchtner.sudoker.model.SudokuBoard;
import de.lorchtner.sudoker.model.SudokuBoardGenerator;
import de.lorchtner.sudoker.model.SudokuCell;
import de.lorchtner.sudoker.view.SudokuBoardView;
import de.lorchtner.sudoker.view.SudokuPanel;
import de.lorchtner.sudoker.view.menu.MenuBar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

public final class GameMaster implements AutoCloseable {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(GameMaster.class);

    private JFrame mainFrame = null;
    private SudokuBoardView boardView = null;
    private SudokuBoard board = null, nextBoard = null;
    private Difficulty difficulty = Difficulty.DEFAULT;

    private final InputEventBus inputBus = new InputEventBus();
    private final OutputEventBus outputBus = new OutputEventBus();

    private final InputEventListener inputEventListener = new InputListener();
    private final ThemeListener themeListener = new ThemeListener();

    {
        inputBus.subscribe(inputEventListener);
        OsThemeDetector.getDetector().registerListener(themeListener);
    }

    /**
     * Should only be called once.
     * Not respecting this is undefined behavior.
     *
     * @param sessionName Must be non-null
     */
    public void startSession(final String sessionName) {
        Objects.requireNonNull(sessionName, "Session must always have a name");

        LOGGER.info("Starting session '{}'...", sessionName);

        this
                .createWindow(sessionName)
                .setVisible(true);

        LOGGER.info("Created window for session '{}'.", sessionName);

        startNewGame();
    }

    /**
     * @param title Must be non-null
     */
    private JFrame createWindow(final String title) {
        final var mainFrame = new JFrame();
        final var menuBar = new MenuBar(inputBus);
        final var sudokuPanel = new SudokuPanel(inputBus, outputBus);

        this.mainFrame = mainFrame;
        this.boardView = sudokuPanel.getBoardView();

        mainFrame.setTitle(title);
        mainFrame.setSize(1024, 786);
        mainFrame.setJMenuBar(menuBar);
        mainFrame.add(sudokuPanel);

        return mainFrame;
    }

    public void startNewGame() {
        board = nextBoard;

        if (board != null) {
            LOGGER.debug(
                    "Starting new game with pre-loaded board {}...",
                    board
            );

            startNewGameOnCurrentBoard();
        } else {
            LOGGER.debug("Starting new game without pre-loaded board...");

            new BoardLoader(difficulty, false).execute();
        }
    }

    private void startNewGameOnCurrentBoard() {
        final var board = Objects.requireNonNull(
                this.board,
                "Cannot start a new game with a null board"
        );

        outputBus.publish(new OutputEvent.BoardCreated(board));
        LOGGER.info("Started new game.");

        this.nextBoard = null;
        new BoardLoader(difficulty, true).execute();
    }

    private void restartGame() {
        final var currentBoard = Objects.requireNonNull(board);
        currentBoard.reset();
        outputBus.publish(OutputEvent.BOARD_RESET);
    }

    /**
     * @param difficulty Must be non-null
     */
    private void selectDifficulty(final Difficulty difficulty) {
        this.difficulty = difficulty;

        // TODO: A loader might already be running;
        //       it might yield a new board after this one,
        //       so the nextBoard would have the wrong difficulty;
        //       solution: Keep reference to loader and cancel when necessary
        this.nextBoard = null;
        new BoardLoader(difficulty, true).execute();
    }

    private SudokuCell getFocusedCell() {
        final var boardView = Objects.requireNonNull(
                this.boardView,
                "Cannot get focused cell when UI is not initialized"
        );
        final var board = Objects.requireNonNull(
                this.board,
                "Cannot get focused cell when there's no board being solved"
        );

        final var focus = boardView.getFocus();

        return board.getCell(focus.rowIndex(), focus.colIndex());
    }

    private void toggleNote(final byte noteValue) {
        final var cell = getFocusedCell();

        if (!cell.isInitial()) {
            cell.toggleNote(noteValue);

            outputBus.publish(OutputEvent.NOTE_TOGGLED);
        } else {
            LOGGER.debug(
                    "Didn't toggle note as currently focused cell is initial"
            );
        }
    }

    private void toggleValue(final byte value) {
        final var cell = getFocusedCell();

        if (!cell.isInitial()) {
            cell.toggleValue(value);

            outputBus.publish(OutputEvent.VALUE_TOGGLED);
        } else {
            LOGGER.debug(
                    "Didn't toggle value as currently focused cell is initial"
            );
        }
    }

    private void checkBoard() {
        final var board = Objects.requireNonNull(
                this.board,
                "Cannot check the board when no board is present"
        );

        final var conflicts = board.computeConflicts();
        outputBus.publish(new OutputEvent.ConflictsUpdated(conflicts));
    }

    @Override
    public void close() {
        OsThemeDetector.getDetector().removeListener(themeListener);
    }

    private final class InputListener implements InputEventListener {
        @Override
        public void onEvent(final InputEvent event) {
            switch (event) {
                case InputEvent.ToggleValue e -> toggleValue(e.value());
                case InputEvent.ToggleNote e -> toggleNote(e.value());

                case InputEvent.SelectDifficulty e ->
                        selectDifficulty(e.difficulty());

                case InputEvent.RestartGame ignored -> restartGame();
                case InputEvent.StartNewGame ignored -> startNewGame();
                case InputEvent.CheckBoard ignored -> checkBoard();

                default -> {
                }
            }
        }
    }

    private final class ThemeListener implements Consumer<Boolean> {
        @Override
        public void accept(Boolean isDark) {
            LOGGER.debug(
                    "System theme changed to theme: "
                            + (isDark ? "dark" : "light")
            );

            SwingUtilities.invokeLater(() -> {
                try {
                    final var laf = isDark
                            ? new FlatDarkLaf()
                            : new FlatLightLaf();

                    UIManager.setLookAndFeel(laf);
                    SwingUtilities.updateComponentTreeUI(mainFrame);
                } catch (UnsupportedLookAndFeelException e) {
                    LOGGER.warn("Failed to apply FlatLaf look-and-feel", e);
                }

                final var event = isDark
                        ? OutputEvent.THEME_CHANGED_TO_DARK
                        : OutputEvent.THEME_CHANGED_TO_LIGHT;
                outputBus.publish(event);
            });
        }
    }

    private final class BoardLoader
            extends SwingWorker<SudokuBoard, SudokuBoard> {
        private final Difficulty difficulty;
        private final boolean isPreload;

        public BoardLoader(Difficulty difficulty, boolean isPreload) {
            this.difficulty = difficulty;
            this.isPreload = isPreload;
        }

        @Override
        protected SudokuBoard doInBackground() {
            LOGGER.debug("Loading board...");
            final var board = SudokuBoardGenerator.generate(difficulty);
            LOGGER.debug("Loaded board {}", board);

            return board;
        }

        @Override
        protected void done() {
            final SudokuBoard board;
            try {
                board = get(0, TimeUnit.NANOSECONDS);
            } catch (InterruptedException | ExecutionException e) {
                if (isPreload) {
                    LOGGER.warn("Failed to pre-load a board", e);
                    return;
                } else {
                    throw new RuntimeException("Failed to load a board", e);
                }
            } catch (TimeoutException e) {
                throw new RuntimeException(
                        "Worker should always return result immediately"
                                + " in done()",
                        e
                );
            }

            if (isPreload) {
                LOGGER.info("Pre-loaded board {}", board);
                GameMaster.this.nextBoard = board;
            } else {
                LOGGER.info("Loaded board {}", board);
                GameMaster.this.board = board;
                startNewGameOnCurrentBoard();
            }
        }
    }
}
