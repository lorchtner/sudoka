package de.lorchtner.sudoker.controller;

import de.lorchtner.sudoker.model.SudokuBoard;
import de.lorchtner.sudoker.model.SudokuCellConflict;

import java.util.Collection;

public sealed interface OutputEvent {
    NoteToggled NOTE_TOGGLED = new NoteToggled();
    ValueToggled VALUE_TOGGLED = new ValueToggled();
    BoardReset BOARD_RESET = new BoardReset();

    ThemeChanged THEME_CHANGED_TO_DARK = new ThemeChanged(true);
    ThemeChanged THEME_CHANGED_TO_LIGHT = new ThemeChanged(false);

    record BoardCreated(SudokuBoard board) implements OutputEvent {}
    record ConflictsUpdated(Collection<SudokuCellConflict> conflicts) implements OutputEvent {}

    final class NoteToggled implements OutputEvent {
        private NoteToggled() {}
    }
    final class ValueToggled implements OutputEvent {
        private ValueToggled() {}
    }
    final class BoardReset implements OutputEvent {
        private BoardReset() {}
    }

    final class ThemeChanged implements OutputEvent {
        private final boolean isDark;

        private ThemeChanged(boolean isDark) {
            this.isDark = isDark;
        }

        public boolean isDark() {
            return isDark;
        }
    }
}
