package de.lorchtner.sudoker;

import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatLightLaf;
import com.jthemedetecor.OsThemeDetector;
import de.lorchtner.sudoker.controller.GameMaster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;

public final class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);
    private static final String APP_TITLE = "Sudoka";

    private Main() {
    }

    public static void main(final String[] args) {
        if (System.getProperty("os.name", "").startsWith("Mac OS")) {
            applyMacSpecificConfig();
        }

        try {
            UIManager.setLookAndFeel(
                    OsThemeDetector.getDetector().isDark()
                            ? new FlatDarkLaf()
                            : new FlatLightLaf()
            );
        } catch (UnsupportedLookAndFeelException e) {
            logger.warn("Failed to apply FlatLaf look-and-feel", e);
        }

        // gameMaster isn't closed because currently,
        // only one session can ever exist
        @SuppressWarnings("resource") final var gameMaster = new GameMaster();
        gameMaster.startSession(APP_TITLE);
    }

    private static void applyMacSpecificConfig() {
        System.setProperty("apple.awt.application.name", APP_TITLE);
        System.setProperty("apple.awt.application.appearance", "system");
        System.setProperty("apple.laf.useScreenMenuBar", "true");

        logger.info("Mac OS-specific configuration applied.");
    }
}
