package de.lorchtner.sudoker.view.menu;

import java.util.Objects;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;

public abstract class AcceleratedAction extends AbstractAction {
    public AcceleratedAction(String name, String keystrokeSpecifier) {
        super(Objects.requireNonNull(name));

        final var keystroke = KeyStroke.getKeyStroke(
                Objects.requireNonNull(keystrokeSpecifier));
        if (keystroke == null) {
            throw new IllegalArgumentException(
                    "Keystroke specifier is malformed: " + keystrokeSpecifier);
        }

        putValue(Action.ACCELERATOR_KEY, keystroke);
    }
}
