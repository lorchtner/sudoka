package de.lorchtner.sudoker.view.menu;

import de.lorchtner.sudoker.controller.InputEvent;
import de.lorchtner.sudoker.controller.InputEventBus;
import de.lorchtner.sudoker.model.Difficulty;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.Objects;

public final class SelectDifficultyAction extends AbstractAction {
    private final InputEvent.SelectDifficulty event;
    private final InputEventBus inputBus;

    /**
     * @param difficulty Must be non-null
     * @param inputBus   Must be non-null
     */
    private SelectDifficultyAction(
            Difficulty difficulty,
            InputEventBus inputBus
    ) {
        super("Difficulty: " + difficulty.getDisplayName());

        this.event = new InputEvent.SelectDifficulty(difficulty);
        this.inputBus = inputBus;
    }

    public Difficulty getDifficulty() {
        return event.difficulty();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        inputBus.publish(event);
    }

    public static SelectDifficultyAction[] createAllVariants(
            InputEventBus inputBus
    ) {
        Objects.requireNonNull(
                inputBus,
                "Must have an input bus to publish events to"
        );

        final var difficulties = Difficulty.values();
        final var actions = new SelectDifficultyAction[difficulties.length];

        for (int i = 0; i < difficulties.length; i++) {
            actions[i] = new SelectDifficultyAction(difficulties[i], inputBus);
        }

        return actions;
    }
}
