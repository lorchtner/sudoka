package de.lorchtner.sudoker.view.menu;

import de.lorchtner.sudoker.controller.InputEvent;
import de.lorchtner.sudoker.controller.InputEventBus;
import de.lorchtner.sudoker.model.Difficulty;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.Objects;

public final class MenuBar extends JMenuBar {
    private final InputEventBus inputBus;

    public MenuBar(final InputEventBus inputBus) {
        super();

        this.inputBus = Objects.requireNonNull(
                inputBus,
                "Must have an input bus to publish events to"
        );

        final var riddleMenu = new JMenu("Riddle");
        riddleMenu.add(new JMenuItem(startNewGameAction()));
        riddleMenu.add(new JMenuItem(restartGameAction()));
        riddleMenu.add(new JMenuItem(checkBoardAction()));
        riddleMenu.add(new JSeparator());
        riddleMenu.add(difficultyMenu());

        add(riddleMenu);
    }

    private JMenu difficultyMenu() {
        final var difficultyMenu = new JMenu("Choose Difficulty");

        for (final var item : difficultyMenuItems()) {
            difficultyMenu.add(item);
        }

        return difficultyMenu;
    }

    private JRadioButtonMenuItem[] difficultyMenuItems() {
        final var actions = SelectDifficultyAction.createAllVariants(inputBus);
        final var buttonGroup = new ButtonGroup();
        final var menuItems = new JRadioButtonMenuItem[actions.length];

        for (int i = 0; i < actions.length; i++) {
            final var action = actions[i];
            final var menuItem = new JRadioButtonMenuItem(action);

            menuItem.setSelected(action.getDifficulty() == Difficulty.DEFAULT);

            buttonGroup.add(menuItem);
            menuItems[i] = menuItem;
        }

        return menuItems;
    }

    private Action startNewGameAction() {
        return new AcceleratedAction("Start Game", "meta N") {
            @Override
            public void actionPerformed(ActionEvent e) {
                inputBus.publish(InputEvent.START_NEW_GAME);
            }
        };
    }

    private Action restartGameAction() {
        return new AcceleratedAction("Restart Game", "meta R") {
            @Override
            public void actionPerformed(ActionEvent e) {
                inputBus.publish(InputEvent.RESTART_GAME);
            }
        };
    }

    private Action checkBoardAction() {
        return new AcceleratedAction("Check Conflicts", "meta C") {
            @Override
            public void actionPerformed(ActionEvent e) {
                inputBus.publish(InputEvent.CHECK_BOARD);
            }
        };
    }
}
