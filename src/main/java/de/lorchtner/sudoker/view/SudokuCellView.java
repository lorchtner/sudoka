package de.lorchtner.sudoker.view;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

public final class SudokuCellView extends JPanel {
    private static final int THICK_BORDER_THICKNESS = 2;
    private static final int THIN_BORDER_THICKNESS = 1;
    private static final String VALUE_FONT_FAMILY = "Serif";
    private static final int VALUE_FONT_SIZE = 28;
    private static final Font INITIAL_VALUE_FONT = new Font(VALUE_FONT_FAMILY, Font.BOLD, VALUE_FONT_SIZE);
    private static final Font VALUE_FONT = new Font(VALUE_FONT_FAMILY, Font.PLAIN, VALUE_FONT_SIZE);

    private final int rowIndex, colIndex;
    private final SudokuCellViewData data;

    private final JTable hintsView;
    private final JLabel valueView;

    public SudokuCellView(
            final int rowIndex,
            final int colIndex,
            final SudokuCellViewData data
    ) {
        super();

        this.rowIndex = rowIndex;
        this.colIndex = colIndex;
        this.data = data;

        this.hintsView = new JTable(3, 3);
        this.valueView = new JLabel();

        var backgroundView = new BackgroundView(
                data != null && data.isFocused(),
                data != null && data.isInvolvedInConflict()
        );

        setLayout(new OverlayLayout(this));
        setBorder();

        hintsView.setOpaque(false);
        hintsView.setShowGrid(false);
        hintsView.setDefaultRenderer(Object.class, hintsRenderer());

        valueView.setOpaque(false);
        valueView.setFont(VALUE_FONT);

        add(valueView);
        add(hintsView);
        add(backgroundView);

        update();
    }

    private void update() {
        if (data == null || data.cell() == null) return;

        if (data.cell().hasValue()) {
            showValue(data.cell().getValue(), data.cell().isInitial());
        } else {
            showNotes(data.cell().getNotes());
        }
    }

    private void showValue(byte value, boolean isInitial) {
        hintsView.setVisible(false);
        valueView.setVisible(true);

        valueView.setFont(isInitial ? INITIAL_VALUE_FONT : VALUE_FONT);
        valueView.setText(Byte.toString(value));
    }

    private void showNotes(byte[] notes) {
        hintsView.setVisible(true);
        valueView.setVisible(false);

        for (int rowIndex = 0; rowIndex < 3; rowIndex++) {
            for (int colIndex = 0; colIndex < 3; colIndex++) {
                final var subcellValue = rowIndex * 3 + colIndex + 1;

                var noteString = "";
                if (arrayContains(notes, (byte) subcellValue)) {
                    noteString = Integer.toString(subcellValue);
                }

                hintsView.setValueAt(noteString, rowIndex, colIndex);
            }
        }
    }

    public void setBorder() {
        final var top = rowIndex % 3 == 0;
        final var left = colIndex % 3 == 0;
        final var bottom = rowIndex % 3 == 2;
        final var right = colIndex % 3 == 2;

        setBorder(BorderFactory.createMatteBorder(
                top ? THICK_BORDER_THICKNESS : THIN_BORDER_THICKNESS,
                left ? THICK_BORDER_THICKNESS : THIN_BORDER_THICKNESS,
                bottom ? THICK_BORDER_THICKNESS : THIN_BORDER_THICKNESS,
                right ? THICK_BORDER_THICKNESS : THIN_BORDER_THICKNESS,
                Color.BLACK
        ));
    }

    private static boolean arrayContains(final byte[] array, final byte needle) {
        for (final var element : array) if (element == needle) return true;

        return false;
    }

    private static TableCellRenderer hintsRenderer() {
        final var renderer = new DefaultTableCellRenderer();
        renderer.setHorizontalAlignment(SwingConstants.CENTER);
        renderer.setOpaque(false);

        return renderer;
    }

    private static final class BackgroundView extends JComponent {
        private static final Color FOCUSED_CONFLICT = new Color(
                Color.PINK.getRed(),
                Color.PINK.getGreen(),
                Color.PINK.getBlue(),
                127
        );
        private static final Color FOCUSED = new Color(127, 127, 127, 127);
        private static final Color CONFLICT = new Color(
                Color.PINK.getRed(),
                Color.PINK.getGreen(),
                Color.PINK.getBlue(),
                63
        );
        private static final Color NO_FOCUS_NO_CONFLICT = new Color(0, 0, 0, 0);

        private final Color color;

        public BackgroundView(
                boolean indicateFocus,
                boolean indicateInvolvementInConflict
        ) {
            Color color;

            if (indicateFocus) {
                if (indicateInvolvementInConflict) {
                    color = FOCUSED_CONFLICT;
                } else {
                    color = FOCUSED;
                }
            } else {
                if (indicateInvolvementInConflict) {
                    color = CONFLICT;
                } else {
                    color = NO_FOCUS_NO_CONFLICT;
                }
            }

            this.color = color;
        }

        @Override
        public void paint(Graphics g) {
            g.setColor(color);
            g.fillRect(0, 0, getWidth(), getHeight());
            g.dispose();
        }
    }
}
