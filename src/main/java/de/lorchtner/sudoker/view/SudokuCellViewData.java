package de.lorchtner.sudoker.view;

import de.lorchtner.sudoker.model.SudokuCell;

import java.util.Objects;

public record SudokuCellViewData(
        SudokuCell cell,
        boolean isFocused,
        boolean isInvolvedInConflict
) {
    public SudokuCellViewData {
        Objects.requireNonNull(cell, "Cell view must always refer to a cell");
    }

    public SudokuCellViewData toFocused(final boolean isFocused) {
        if (isFocused == this.isFocused) return this;

        return new SudokuCellViewData(cell, isFocused, isInvolvedInConflict);
    }

    public SudokuCellViewData toInvolvedInConflict(
            final boolean isInvolvedInConflict
    ) {
        if (isInvolvedInConflict == this.isInvolvedInConflict) return this;

        return new SudokuCellViewData(cell, isFocused, isInvolvedInConflict);
    }
}
