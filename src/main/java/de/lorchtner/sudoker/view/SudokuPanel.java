package de.lorchtner.sudoker.view;

import de.lorchtner.sudoker.controller.InputEventBus;
import de.lorchtner.sudoker.controller.OutputEventBus;

import javax.swing.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public final class SudokuPanel extends JPanel {
    private final SudokuBoardView boardView;

    public SudokuPanel(InputEventBus inputBus, OutputEventBus outputBus) {
        super();

        this.boardView = new SudokuBoardView(inputBus, outputBus);

        addKeybindings(inputBus);
        addComponentListener(new CompListener());

        add(boardView);

        // dirty workaround to initially set the board's proper size;
        // could not find out in a reasonable amount of time
        // why the board's initial height is way too small
        Thread.ofVirtual().start(() -> {
            try {
                while (getHeight() == 0) Thread.sleep(10);
            } catch (InterruptedException e) {
                // this thread should never be interrupted
                throw new RuntimeException(e);
            } finally {
                SwingUtilities.invokeLater(this::refreshBoardSize);
            }
        });
    }

    public SudokuBoardView getBoardView() {
        return boardView;
    }

    private void refreshBoardSize() {
        final var sideLen = Math.min(getWidth(), getHeight());
        final var cellSideLen = sideLen / 9;

        boardView.setRowHeight(cellSideLen);

        for (int colIndex = 0; colIndex < 9; colIndex++) {
            boardView
                    .getColumnModel()
                    .getColumn(colIndex)
                    .setPreferredWidth(cellSideLen);
        }
    }

    private void addKeybindings(InputEventBus inputBus) {
        final var inputMap = getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        final var actionMap = getActionMap();

        InputMapPopulator
                .populate(inputMap);
        new ActionMapPopulator(inputBus)
                .populate(actionMap);
    }

    private final class CompListener extends ComponentAdapter {
        @Override
        public void componentResized(ComponentEvent e) {
            refreshBoardSize();
        }
    }
}
