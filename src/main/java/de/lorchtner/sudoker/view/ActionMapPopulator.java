package de.lorchtner.sudoker.view;

import de.lorchtner.sudoker.controller.InputEvent;
import de.lorchtner.sudoker.controller.InputEventBus;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.Objects;

/**
 * Populates an {@link ActionMap} with all {@link Action}s
 * that are available in a Sudoka game.
 */
public final class ActionMapPopulator {
    private final InputEventBus eventBus;

    public ActionMapPopulator(InputEventBus eventBus) {
        this.eventBus = Objects.requireNonNull(eventBus);
    }

    public void populate(ActionMap actionMap) {
        Objects.requireNonNull(actionMap, "Cannot populate a null ActionMap");

        actionMap.put(Actions.MOVE_FOCUS_UP, moveFocus(Direction.UP, 1));
        actionMap.put(Actions.MOVE_FOCUS_DOWN, moveFocus(Direction.DOWN, 1));
        actionMap.put(Actions.MOVE_FOCUS_LEFT, moveFocus(Direction.LEFT, 1));
        actionMap.put(Actions.MOVE_FOCUS_RIGHT, moveFocus(Direction.RIGHT, 1));

        actionMap.put(Actions.MOVE_FOCUS_FAR_UP, moveFocus(Direction.UP, 4));
        actionMap.put(Actions.MOVE_FOCUS_FAR_DOWN, moveFocus(Direction.DOWN, 4));
        actionMap.put(Actions.MOVE_FOCUS_FAR_LEFT, moveFocus(Direction.LEFT, 4));
        actionMap.put(Actions.MOVE_FOCUS_FAR_RIGHT, moveFocus(Direction.RIGHT, 4));

        actionMap.put(Actions.TOGGLE_HINT_1, toggleHint((byte) 1));
        actionMap.put(Actions.TOGGLE_HINT_2, toggleHint((byte) 2));
        actionMap.put(Actions.TOGGLE_HINT_3, toggleHint((byte) 3));
        actionMap.put(Actions.TOGGLE_HINT_4, toggleHint((byte) 4));
        actionMap.put(Actions.TOGGLE_HINT_5, toggleHint((byte) 5));
        actionMap.put(Actions.TOGGLE_HINT_6, toggleHint((byte) 6));
        actionMap.put(Actions.TOGGLE_HINT_7, toggleHint((byte) 7));
        actionMap.put(Actions.TOGGLE_HINT_8, toggleHint((byte) 8));
        actionMap.put(Actions.TOGGLE_HINT_9, toggleHint((byte) 9));

        actionMap.put(Actions.TOGGLE_VALUE_1, toggleValue((byte) 1));
        actionMap.put(Actions.TOGGLE_VALUE_2, toggleValue((byte) 2));
        actionMap.put(Actions.TOGGLE_VALUE_3, toggleValue((byte) 3));
        actionMap.put(Actions.TOGGLE_VALUE_4, toggleValue((byte) 4));
        actionMap.put(Actions.TOGGLE_VALUE_5, toggleValue((byte) 5));
        actionMap.put(Actions.TOGGLE_VALUE_6, toggleValue((byte) 6));
        actionMap.put(Actions.TOGGLE_VALUE_7, toggleValue((byte) 7));
        actionMap.put(Actions.TOGGLE_VALUE_8, toggleValue((byte) 8));
        actionMap.put(Actions.TOGGLE_VALUE_9, toggleValue((byte) 9));
    }

    private PublishEventAction moveFocus(Direction direction, int cellDistance) {
        final var event = new InputEvent.MoveFocus(direction, cellDistance);
        return new PublishEventAction(event);
    }

    private PublishEventAction toggleHint(byte value) {
        final var event = new InputEvent.ToggleNote(value);
        return new PublishEventAction(event);
    }

    private PublishEventAction toggleValue(byte value) {
        final var event = new InputEvent.ToggleValue(value);
        return new PublishEventAction(event);
    }

    private final class PublishEventAction extends AbstractAction {
        private final InputEvent event;

        public PublishEventAction(InputEvent event) {
            this.event = event;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            eventBus.publish(event);
        }
    }
}
