package de.lorchtner.sudoker.view;

import de.lorchtner.sudoker.controller.InputEvent;
import de.lorchtner.sudoker.controller.InputEventBus;
import de.lorchtner.sudoker.controller.InputEventListener;
import de.lorchtner.sudoker.controller.OutputEvent;
import de.lorchtner.sudoker.controller.OutputEventBus;
import de.lorchtner.sudoker.controller.OutputEventListener;
import de.lorchtner.sudoker.model.SudokuBoard;
import de.lorchtner.sudoker.model.SudokuCellConflict;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;

public final class SudokuBoardView extends JTable {
    private static final TableModel TABLE_MODEL = tableModel();
    private static final TableCellRenderer TABLE_CELL_RENDERER = tableCellRenderer();

    @SuppressWarnings("FieldCanBeLocal")
    private final InputListener inputListener = new InputListener();
    @SuppressWarnings("FieldCanBeLocal")
    private final OutputListener outputListener = new OutputListener();

    private CellLocation focus = CellLocation.ZERO;

    {
        setDefaultRenderer(SudokuCellViewData.class, TABLE_CELL_RENDERER);
        setGridColor(Color.BLACK);

        // make <meta C> keybinding activate the menu item;
        // by default, tables capture the keystroke to copy row content;
        // fix source: https://stackoverflow.com/a/16139103
        this
                .getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
                .getParent()
                .remove(KeyStroke.getKeyStroke("meta C"));

        addMouseListener(new MouseListener());
    }

    public SudokuBoardView(
            final InputEventBus inputBus,
            final OutputEventBus outputBus
    ) {
        super(TABLE_MODEL);

        inputBus.subscribe(inputListener);
        outputBus.subscribe(outputListener);
    }

    public CellLocation getFocus() {
        return focus;
    }

    private void setFocus(final CellLocation focus) {
        Objects.requireNonNull(focus, "Cannot move focus to a null focus");

        if (focus.equals(this.focus)) return;

        final var unfocusedCellData = getCellDataAt(this.focus).toFocused(false);
        final var focusedCellData = getCellDataAt(focus).toFocused(true);

        setCellDataAt(this.focus, unfocusedCellData);
        setCellDataAt(focus, focusedCellData);

        this.focus = focus;
    }

    private SudokuCellViewData getCellDataAt(
            final int rowIndex,
            final int colIndex
    ) {
        final var cellData = getValueAt(rowIndex, colIndex);
        return (SudokuCellViewData) cellData;
    }

    /**
     * @param location Must be non-null
     */
    private SudokuCellViewData getCellDataAt(CellLocation location) {
        return getCellDataAt(location.rowIndex(), location.colIndex());
    }

    /**
     * @param location Must be non-null
     * @param data     Must be non-null
     */
    private void setCellDataAt(
            final CellLocation location,
            final SudokuCellViewData data
    ) {
        setValueAt(data, location.rowIndex(), location.colIndex());
    }

    private void refreshCellAt(final int rowIndex, final int colIndex) {
        setValueAt(getValueAt(rowIndex, colIndex), rowIndex, colIndex);
    }

    /**
     * @param location Must be non-null
     */
    private void refreshCellAt(final CellLocation location) {
        refreshCellAt(location.rowIndex(), location.colIndex());
    }

    private void refreshFocusedCell() {
        refreshCellAt(focus.rowIndex(), focus.colIndex());
    }

    private void refreshAllCells() {
        for (int rowIndex = 0; rowIndex < 9; rowIndex++) {
            for (int colIndex = 0; colIndex < 9; colIndex++) {
                refreshCellAt(rowIndex, colIndex);
            }
        }
    }

    private void populateCells(final SudokuBoard board) {
        for (int rowIndex = 0; rowIndex < 9; rowIndex++) {
            for (int colIndex = 0; colIndex < 9; colIndex++) {
                final var cell = board.getCell(rowIndex, colIndex);
                final var cellIsInitiallyFocused = rowIndex == focus.rowIndex()
                        && colIndex == focus.colIndex();
                final var viewData = new SudokuCellViewData(
                        cell,
                        cellIsInitiallyFocused,
                        false
                );

                setValueAt(viewData, rowIndex, colIndex);
            }
        }
    }

    private void updateConflicts(
            final Collection<SudokuCellConflict> conflicts
    ) {
        if (conflicts == null || conflicts.isEmpty()) {
            // remove all conflict indications

            for (int rowIndex = 0; rowIndex < 9; rowIndex++) {
                for (int colIndex = 0; colIndex < 9; colIndex++) {
                    final var data = getCellDataAt(rowIndex, colIndex);
                    if (!data.isInvolvedInConflict()) continue;

                    final var updatedData = data.toInvolvedInConflict(false);
                    setValueAt(updatedData, rowIndex, colIndex);
                }
            }

            return;
        }

        final var conflictingCells = new HashSet<CellLocation>();

        for (final var conflict : conflicts) {
            for (final var cellInfo : conflict.getCells()) {
                conflictingCells.add(cellInfo.getLocation());
            }
        }

        for (int rowIndex = 0; rowIndex < 9; rowIndex++) {
            for (int colIndex = 0; colIndex < 9; colIndex++) {
                final var location = new CellLocation(rowIndex, colIndex);
                final var data = getCellDataAt(location);

                if (conflictingCells.contains(location)) {
                    if (!data.isInvolvedInConflict()) {
                        setCellDataAt(location, data.toInvolvedInConflict(true));
                    }
                } else {
                    if (data.isInvolvedInConflict()) {
                        setCellDataAt(location, data.toInvolvedInConflict(false));
                    }
                }
            }
        }
    }

    private final class OutputListener implements OutputEventListener {
        @Override
        public void onEvent(final OutputEvent event) {
            switch (event) {
                case OutputEvent.BoardCreated e -> populateCells(e.board());
                case OutputEvent.BoardReset ignored -> refreshAllCells();
                case OutputEvent.ValueToggled ignored -> refreshFocusedCell();
                case OutputEvent.NoteToggled ignored -> refreshFocusedCell();
                case OutputEvent.ConflictsUpdated e ->
                        updateConflicts(e.conflicts());

                default -> {
                }
            }
        }
    }

    private final class InputListener implements InputEventListener {
        @Override
        public void onEvent(final InputEvent event) {
            if (!(event instanceof InputEvent.MoveFocus e)) return;

            final var movedFocus = focus.moved(e.direction(), e.cellDistance());

            if (!movedFocus.equals(focus)) setFocus(movedFocus);
        }
    }

    private final class MouseListener extends MouseAdapter {
        @Override
        public void mousePressed(final MouseEvent e) {
            shiftFocus(e);
        }

        @Override
        public void mouseReleased(final MouseEvent e) {
            shiftFocus(e);
        }

        private void shiftFocus(final MouseEvent e) {
            e.consume();

            final int x = e.getX(), y = e.getY();
            final int xMax = getWidth(), yMax = getHeight();
            final int rowIndex = 9 * y / yMax, colIndex = 9 * x / xMax;

            if (rowIndex >= 0 && colIndex >= 0 && rowIndex < 9 && colIndex < 9) {
                setFocus(new CellLocation(rowIndex, colIndex));
            }
        }
    }

    private static TableModel tableModel() {
        return new DefaultTableModel(9, 9) {
            @Override
            public Class<?> getColumnClass(final int columnIndex) {
                return SudokuCellViewData.class;
            }
        };
    }

    private static TableCellRenderer tableCellRenderer() {
        return new DefaultTableCellRenderer() {
            {
                // TODO: Does this do anything?
                //setHorizontalAlignment(SwingConstants.CENTER);
            }

            @Override
            public Component getTableCellRendererComponent(
                    final JTable table,
                    final Object value,
                    final boolean isSelected,
                    final boolean hasFocus,
                    final int row,
                    final int column
            ) {
                final var viewData = (SudokuCellViewData) value;
                return new SudokuCellView(row, column, viewData);
            }
        };
    }
}
