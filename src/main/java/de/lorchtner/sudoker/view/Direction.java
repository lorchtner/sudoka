package de.lorchtner.sudoker.view;

public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT,
}
