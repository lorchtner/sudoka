package de.lorchtner.sudoker.view;

import de.lorchtner.sudoker.model.Assert;

import java.util.Objects;

public record CellLocation(
        int rowIndex,
        int colIndex
) {
    public static final CellLocation ZERO = new CellLocation(0, 0);

    public CellLocation {
        Assert.boardIndex(rowIndex);
        Assert.boardIndex(colIndex);
    }

    public CellLocation moved(final Direction direction, final int cellDistance) {
        Objects.requireNonNull(direction, "Cannot move in a null direction");
        Assert.distance(cellDistance);

        if (cellDistance == 0) return this;

        int targetRowIndex = rowIndex, targetColIndex = colIndex;
        switch (direction) {
            case UP -> targetRowIndex = Math.max(0, rowIndex - cellDistance);
            case DOWN -> targetRowIndex = Math.min(8, rowIndex + cellDistance);
            case LEFT -> targetColIndex = Math.max(0, colIndex - cellDistance);
            case RIGHT -> targetColIndex = Math.min(8, colIndex + cellDistance);
        }

        if (targetRowIndex == rowIndex && targetColIndex == colIndex) {
            return this;
        } else {
            return new CellLocation(targetRowIndex, targetColIndex);
        }
    }
}
