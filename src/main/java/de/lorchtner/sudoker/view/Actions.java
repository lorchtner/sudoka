package de.lorchtner.sudoker.view;

public final class Actions {
    private Actions() {
    }

    public static final String
            TOGGLE_HINT_1 = "TOGGLE_HINT_1",
            TOGGLE_HINT_2 = "TOGGLE_HINT_2",
            TOGGLE_HINT_3 = "TOGGLE_HINT_3",
            TOGGLE_HINT_4 = "TOGGLE_HINT_4",
            TOGGLE_HINT_5 = "TOGGLE_HINT_5",
            TOGGLE_HINT_6 = "TOGGLE_HINT_6",
            TOGGLE_HINT_7 = "TOGGLE_HINT_7",
            TOGGLE_HINT_8 = "TOGGLE_HINT_8",
            TOGGLE_HINT_9 = "TOGGLE_HINT_9",

            TOGGLE_VALUE_1 = "TOGGLE_VALUE_1",
            TOGGLE_VALUE_2 = "TOGGLE_VALUE_2",
            TOGGLE_VALUE_3 = "TOGGLE_VALUE_3",
            TOGGLE_VALUE_4 = "TOGGLE_VALUE_4",
            TOGGLE_VALUE_5 = "TOGGLE_VALUE_5",
            TOGGLE_VALUE_6 = "TOGGLE_VALUE_6",
            TOGGLE_VALUE_7 = "TOGGLE_VALUE_7",
            TOGGLE_VALUE_8 = "TOGGLE_VALUE_8",
            TOGGLE_VALUE_9 = "TOGGLE_VALUE_9",

            MOVE_FOCUS_UP = "MOVE_FOCUS_UP",
            MOVE_FOCUS_DOWN = "MOVE_FOCUS_DOWN",
            MOVE_FOCUS_LEFT = "MOVE_FOCUS_LEFT",
            MOVE_FOCUS_RIGHT = "MOVE_FOCUS_RIGHT",

            MOVE_FOCUS_FAR_UP = "MOVE_FOCUS_FAR_UP",
            MOVE_FOCUS_FAR_DOWN = "MOVE_FOCUS_FAR_DOWN",
            MOVE_FOCUS_FAR_LEFT = "MOVE_FOCUS_FAR_LEFT",
            MOVE_FOCUS_FAR_RIGHT = "MOVE_FOCUS_FAR_RIGHT",

            CHECK_BOARD = "CHECK_BOARD";
}
