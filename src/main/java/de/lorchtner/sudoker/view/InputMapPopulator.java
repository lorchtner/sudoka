package de.lorchtner.sudoker.view;

import javax.swing.*;

public final class InputMapPopulator {
    private InputMapPopulator() {
    }

    public static void populate(InputMap inputMap) {
        inputMap.put(KeyStroke.getKeyStroke("1"), Actions.TOGGLE_HINT_1);
        inputMap.put(KeyStroke.getKeyStroke("2"), Actions.TOGGLE_HINT_2);
        inputMap.put(KeyStroke.getKeyStroke("3"), Actions.TOGGLE_HINT_3);
        inputMap.put(KeyStroke.getKeyStroke("4"), Actions.TOGGLE_HINT_4);
        inputMap.put(KeyStroke.getKeyStroke("5"), Actions.TOGGLE_HINT_5);
        inputMap.put(KeyStroke.getKeyStroke("6"), Actions.TOGGLE_HINT_6);
        inputMap.put(KeyStroke.getKeyStroke("7"), Actions.TOGGLE_HINT_7);
        inputMap.put(KeyStroke.getKeyStroke("8"), Actions.TOGGLE_HINT_8);
        inputMap.put(KeyStroke.getKeyStroke("9"), Actions.TOGGLE_HINT_9);

        inputMap.put(KeyStroke.getKeyStroke("W"), Actions.TOGGLE_HINT_1);
        inputMap.put(KeyStroke.getKeyStroke("E"), Actions.TOGGLE_HINT_2);
        inputMap.put(KeyStroke.getKeyStroke("R"), Actions.TOGGLE_HINT_3);
        inputMap.put(KeyStroke.getKeyStroke("S"), Actions.TOGGLE_HINT_4);
        inputMap.put(KeyStroke.getKeyStroke("D"), Actions.TOGGLE_HINT_5);
        inputMap.put(KeyStroke.getKeyStroke("F"), Actions.TOGGLE_HINT_6);
        inputMap.put(KeyStroke.getKeyStroke("X"), Actions.TOGGLE_HINT_7);
        inputMap.put(KeyStroke.getKeyStroke("C"), Actions.TOGGLE_HINT_8);
        inputMap.put(KeyStroke.getKeyStroke("V"), Actions.TOGGLE_HINT_9);

        inputMap.put(KeyStroke.getKeyStroke("shift 1"), Actions.TOGGLE_VALUE_1);
        inputMap.put(KeyStroke.getKeyStroke("shift 2"), Actions.TOGGLE_VALUE_2);
        inputMap.put(KeyStroke.getKeyStroke("shift 3"), Actions.TOGGLE_VALUE_3);
        inputMap.put(KeyStroke.getKeyStroke("shift 4"), Actions.TOGGLE_VALUE_4);
        inputMap.put(KeyStroke.getKeyStroke("shift 5"), Actions.TOGGLE_VALUE_5);
        inputMap.put(KeyStroke.getKeyStroke("shift 6"), Actions.TOGGLE_VALUE_6);
        inputMap.put(KeyStroke.getKeyStroke("shift 7"), Actions.TOGGLE_VALUE_7);
        inputMap.put(KeyStroke.getKeyStroke("shift 8"), Actions.TOGGLE_VALUE_8);
        inputMap.put(KeyStroke.getKeyStroke("shift 9"), Actions.TOGGLE_VALUE_9);

        inputMap.put(KeyStroke.getKeyStroke("shift W"), Actions.TOGGLE_VALUE_1);
        inputMap.put(KeyStroke.getKeyStroke("shift E"), Actions.TOGGLE_VALUE_2);
        inputMap.put(KeyStroke.getKeyStroke("shift R"), Actions.TOGGLE_VALUE_3);
        inputMap.put(KeyStroke.getKeyStroke("shift S"), Actions.TOGGLE_VALUE_4);
        inputMap.put(KeyStroke.getKeyStroke("shift D"), Actions.TOGGLE_VALUE_5);
        inputMap.put(KeyStroke.getKeyStroke("shift F"), Actions.TOGGLE_VALUE_6);
        inputMap.put(KeyStroke.getKeyStroke("shift X"), Actions.TOGGLE_VALUE_7);
        inputMap.put(KeyStroke.getKeyStroke("shift C"), Actions.TOGGLE_VALUE_8);
        inputMap.put(KeyStroke.getKeyStroke("shift V"), Actions.TOGGLE_VALUE_9);

        inputMap.put(KeyStroke.getKeyStroke("K"), Actions.MOVE_FOCUS_UP);
        inputMap.put(KeyStroke.getKeyStroke("J"), Actions.MOVE_FOCUS_DOWN);
        inputMap.put(KeyStroke.getKeyStroke("H"), Actions.MOVE_FOCUS_LEFT);
        inputMap.put(KeyStroke.getKeyStroke("L"), Actions.MOVE_FOCUS_RIGHT);

        inputMap.put(KeyStroke.getKeyStroke("shift K"), Actions.MOVE_FOCUS_FAR_UP);
        inputMap.put(KeyStroke.getKeyStroke("shift J"), Actions.MOVE_FOCUS_FAR_DOWN);
        inputMap.put(KeyStroke.getKeyStroke("shift H"), Actions.MOVE_FOCUS_FAR_LEFT);
        inputMap.put(KeyStroke.getKeyStroke("shift L"), Actions.MOVE_FOCUS_FAR_RIGHT);
    }
}
