package de.lorchtner.sudoker.model;

public final class IllegalModificationException extends RuntimeException {
    public IllegalModificationException(String message) {
        super(message);
    }
}
