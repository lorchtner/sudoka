package de.lorchtner.sudoker.model;

import java.util.Arrays;
import java.util.Objects;

public final class SudokuCell {
    /**
     * Value of 0 signifies an absent value.
     */
    private byte value;
    private final boolean[] notes;
    private boolean isInitial;

    /**
     * @param initialValue The cell's initial value
     *                     or 0 to create a cell without a value.
     */
    public SudokuCell(byte initialValue) {
        if (initialValue != 0) {
            Assert.value(initialValue);
        }

        this.value = initialValue;
        this.notes = new boolean[] { false, false, false, false, false, false, false, false, false };
        this.isInitial = initialValue != 0;
    }

    /**
     * Constructs a cell without an initial value.
     */
    public SudokuCell() {
        this((byte) 0);
    }

    /**
     * @return The cell's value or 0 if there is none.
     */
    public byte getValue() {
        return this.value;
    }

    /**
     * @param value The cell's new value.
     */
    public void setValue(byte value) {
        assertIsMutable();
        Assert.value(value);

        this.value = value;
    }

    public void toggleValue(byte value) {
        if (this.value == value) {
            clearValue();
        } else {
            setValue(value);
        }
    }

    public void clearValue() {
        assertIsMutable();

        this.value = 0;
    }

    public boolean hasValue() {
        return value != 0;
    }

    public byte[] getNotes() {
        var firstNoteIndex = -1;
        var noteCount = 0;
        for (int i = 0; i < 9; i++) {
            if (notes[i]) {
                if (firstNoteIndex == -1) {
                    firstNoteIndex = i;
                }

                noteCount++;
            }
        }

        if (noteCount == 0)
            return new byte[0];

        final var result = new byte[noteCount];
        var resultIndex = 0;
        for (int i = firstNoteIndex; i < 9; i++) {
            if (notes[i]) {
                result[resultIndex++] = (byte) (i + 1);
            }
        }

        return result;
    }

    public void toggleNote(byte noteValue) {
        assertIsMutable();
        Assert.value(noteValue);

        final var noteIndex = noteValue - 1;
        notes[noteIndex] = !notes[noteIndex];
    }

    public void clearNotes() {
        assertIsMutable();

        for (int i = 0; i < 9; i++) {
            notes[i] = false;
        }
    }

    public boolean isInitial() {
        return isInitial;
    }

    public void setInitial(boolean isInitial) {
        this.isInitial = isInitial;
    }

    public void reset() {
        clearValue();
        clearNotes();
    }

    /**
     * Throw an exception if this cell is not allowed to be mutated
     * (i.e. if it is an initial cell).
     */
    private void assertIsMutable() {
        if (isInitial) {
            throw new IllegalModificationException(
                    "Initial Sudoku cells may never be mutated");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        SudokuCell that = (SudokuCell) o;
        return value == that.value && Arrays.equals(notes, that.notes);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(value);
        result = 31 * result + Arrays.hashCode(notes);
        return result;
    }
}
