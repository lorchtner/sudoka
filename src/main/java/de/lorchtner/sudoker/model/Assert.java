package de.lorchtner.sudoker.model;

public final class Assert {
    private Assert() {
    }

    /**
     * Throws an exception
     * if the parameter is not a valid value for a cell in a Sudoku grid.
     *
     * @param value The Sudoku grid value that must fulfil 0 < value < 10.
     */
    public static void value(final byte value) {
        if (value >= 1 && value <= 9) return;

        throw new IllegalArgumentException(
                "Values in a Sudoku grid must always be between 1 and 9."
                        + "Actual value: "
                        + value
                        + ".");
    }

    /**
     * Throws an exception * if the parameter is not a valid index
     * for the row/column of a cell in a 9x9 Sudoku grid.
     *
     * @param boardIndex The Sudoku grid boardIndex that must fulfil 0 <= boardIndex < 9.
     */
    public static void boardIndex(final int boardIndex) {
        if (boardIndex >= 0 && boardIndex < 9) return;

        throw new IllegalArgumentException(
                "Indices of the cells in a Sudoku grid"
                        + "must always be between 0 and 8."
                        + "Actual boardIndex: "
                        + boardIndex
                        + "."
        );
    }

    public static void distance(final int distance) {
        if (distance >= 0) return;

        throw new IllegalArgumentException("Distances cannot be negative.");
    }
}
