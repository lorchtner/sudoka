package de.lorchtner.sudoker.model;

import de.sfuhrm.sudoku.Riddle;

import java.util.HashSet;

public final class SudokuBoard {
    private final SudokuCell[][] cells;

    /**
     * Constructs an empty board.
     */
    public SudokuBoard() {
        final var cells = new SudokuCell[9][9];
        for (int rowIndex = 0; rowIndex < 9; rowIndex++) {
            for (int colIndex = 0; colIndex < 9; colIndex++) {
                cells[rowIndex][colIndex] = new SudokuCell();
            }
        }

        this.cells = cells;
    }

    /**
     * Constructs a board with the values from a {@link Riddle}.
     */
    public SudokuBoard(Riddle riddle) {
        final var cells = new SudokuCell[9][9];
        for (int rowIndex = 0; rowIndex < 9; rowIndex++) {
            for (int colIndex = 0; colIndex < 9; colIndex++) {
                final var cellValue = riddle.get(rowIndex, colIndex);
                final var cell = new SudokuCell(cellValue);
                cells[rowIndex][colIndex] = cell;
            }
        }

        this.cells = cells;
    }

    public SudokuCell getCell(int rowIndex, int colIndex) {
        return cells[rowIndex][colIndex];
    }

    /**
     * Counts the total number of filled cells
     * (cells that have a value).
     * This includes "initial" cells
     * (those that have values at the start of the game).
     */
    public int countFilledCells() {
        int count = 0;

        for (final var row : cells) {
            for (final var cell : row) {
                if (cell.hasValue())
                    count++;
            }
        }

        return count;
    }

    public boolean isFullyFilled() {
        for (final var row : cells) {
            for (final var cell : row) {
                if (!cell.hasValue())
                    return false;
            }
        }

        return true;
    }

    private SudokuCellConflict createPotentialConflictInRow(
            final int rowIndex,
            final int testColIndex
    ) {
        final var testCell = cells[rowIndex][testColIndex];
        if (!testCell.hasValue()) return null;

        SudokuCellConflict conflict = null;

        for (int compareColIndex = testColIndex + 1; compareColIndex < 9; compareColIndex++) {
            final var compareCell = cells[rowIndex][compareColIndex];

            if (testCell.getValue() != compareCell.getValue()) continue;

            final var compareCellInfo = new SudokuCellConflict.CellInfo(
                    rowIndex,
                    compareColIndex,
                    compareCell
            );

            if (conflict != null) {
                conflict.addCell(compareCellInfo);
            } else {
                final var testCellInfo = new SudokuCellConflict.CellInfo(
                        rowIndex,
                        testColIndex,
                        testCell
                );

                conflict = new SudokuCellConflict(
                        testCellInfo,
                        compareCellInfo
                );
            }
        }

        return conflict;
    }

    private SudokuCellConflict createPotentialConflictInCol(
            final int testRowIndex,
            final int colIndex
    ) {
        final var testCell = cells[testRowIndex][colIndex];
        if (!testCell.hasValue()) return null;

        SudokuCellConflict conflict = null;

        for (int compareRowIndex = testRowIndex + 1; compareRowIndex < 9; compareRowIndex++) {
            final var compareCell = cells[compareRowIndex][colIndex];

            if (testCell.getValue() != compareCell.getValue()) continue;

            final var compareCellInfo = new SudokuCellConflict.CellInfo(
                    compareRowIndex,
                    colIndex,
                    compareCell
            );

            if (conflict != null) {
                conflict.addCell(compareCellInfo);
            } else {
                final var testCellInfo = new SudokuCellConflict.CellInfo(
                        testRowIndex,
                        colIndex,
                        testCell
                );

                conflict = new SudokuCellConflict(
                        testCellInfo,
                        compareCellInfo
                );
            }
        }
        return conflict;
    }

    public HashSet<SudokuCellConflict> computeConflicts() {
        final var result = new HashSet<SudokuCellConflict>();

        // find conflicts in rows
        for (int rowIndex = 0; rowIndex < 9; rowIndex++) {
            for (int testColIndex = 0; testColIndex < 8; testColIndex++) {
                final var conflict =
                        createPotentialConflictInRow(rowIndex, testColIndex);
                if (conflict != null) result.add(conflict);
            }
        }

        // find conflicts in cols
        for (int colIndex = 0; colIndex < 9; colIndex++) {
            for (int testRowIndex = 0; testRowIndex < 8; testRowIndex++) {
                final var conflict =
                        createPotentialConflictInCol(testRowIndex, colIndex);
                if (conflict != null) result.add(conflict);
            }
        }

        // find conflicts in blocks - no idea how to make this less ugly
        // without introducing more ugliness
        for (
                int firstBlockRowIndex = 0;
                firstBlockRowIndex < 9;
                firstBlockRowIndex += 3
        ) {
            for (
                    int firstBlockColIndex = 0;
                    firstBlockColIndex < 9;
                    firstBlockColIndex += 3
            ) {
                for (
                        int testRowIndex = firstBlockRowIndex;
                        testRowIndex < firstBlockRowIndex + 3;
                        testRowIndex++
                ) {
                    for (
                            int testColIndex = firstBlockColIndex;
                            testColIndex < firstBlockColIndex + 3;
                            testColIndex++
                    ) {
                        final var testCell = cells[testRowIndex][testColIndex];
                        if (!testCell.hasValue()) continue;

                        SudokuCellConflict conflict = null;

                        final var testCellIsInLastBlockCol =
                                (testColIndex == firstBlockColIndex + 2);

                        for (
                                int compareRowIndex = testCellIsInLastBlockCol
                                        ? testRowIndex + 1
                                        : testRowIndex;
                                compareRowIndex < firstBlockRowIndex + 3;
                                compareRowIndex++
                        ) {
                            final var startInFirstBlockCol =
                                    (compareRowIndex > testRowIndex);

                            for (
                                    int compareColIndex = startInFirstBlockCol
                                            ? firstBlockColIndex
                                            : testColIndex + 1;
                                    compareColIndex < firstBlockColIndex + 3;
                                    compareColIndex++
                            ) {
                                final var compareCell =
                                        cells[compareRowIndex][compareColIndex];
                                if (testCell.getValue() != compareCell.getValue())
                                    continue;

                                final var compareCellInfo =
                                        new SudokuCellConflict.CellInfo(
                                                compareRowIndex,
                                                compareColIndex,
                                                compareCell
                                        );

                                if (conflict != null) {
                                    conflict.addCell(compareCellInfo);
                                } else {
                                    final var testCellInfo =
                                            new SudokuCellConflict.CellInfo(
                                                    testRowIndex,
                                                    testColIndex,
                                                    testCell
                                            );

                                    conflict = new SudokuCellConflict(
                                            testCellInfo,
                                            compareCellInfo
                                    );
                                }
                            }
                        }

                        if (conflict != null) result.add(conflict);
                    }
                }
            }
        }

        return result;
    }

    public void reset() {
        for (final var row : cells) {
            for (final var cell : row) {
                if (!cell.isInitial())
                    cell.reset();
            }
        }
    }
}
