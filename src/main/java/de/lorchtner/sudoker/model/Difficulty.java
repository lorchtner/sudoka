package de.lorchtner.sudoker.model;

public enum Difficulty {
    LAME("Lame"),
    EASY("Easy"),
    MEDIUM("Medium"),
    HARD("Hard"),
    INSANE("INSANE"),
    ;

    public static final Difficulty DEFAULT = Difficulty.MEDIUM;

    private final String displayName;

    private Difficulty(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
