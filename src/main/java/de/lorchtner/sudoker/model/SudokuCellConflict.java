package de.lorchtner.sudoker.model;

import de.lorchtner.sudoker.view.CellLocation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public final class SudokuCellConflict {
    private final ArrayList<CellInfo> cells;

    SudokuCellConflict(CellInfo cell1, CellInfo cell2) {
        // initial capacity based on the assumption
        // that a conflict will almost always be between two cells
        this.cells = new ArrayList<>(2);

        addCell(cell1);
        addCell(cell2);
    }

    public List<CellInfo> getCells() {
        return Collections.unmodifiableList(cells);
    }

    void addCell(CellInfo cell) {
        if (cells.size() == 9) {
            throw new IllegalStateException(
                    "Having more than 9 cells in a conflict makes no sense");
        }
        Objects.requireNonNull(cell, "Can only add non-null cells");

        cells.add(cell);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SudokuCellConflict that = (SudokuCellConflict) o;
        return Objects.equals(cells, that.cells);
    }

    @Override
    public int hashCode() {
        return cells.hashCode();
    }

    @Override
    public String toString() {
        final var result = new StringBuilder("SudokuCellConflict[");

        for (final var cell : cells) result.append(cell);

        return result.append("]").toString();
    }

    public record CellInfo(int rowIndex, int colIndex, SudokuCell cell) {
        public CellInfo {
            Assert.boardIndex(rowIndex);
            Assert.boardIndex(colIndex);
            Objects.requireNonNull(cell);
        }

        public CellLocation getLocation() {
            return new CellLocation(rowIndex, colIndex);
        }

        @Override
        public String toString() {
            return "CellInfo{"
                    + "(row, col)=("
                    + rowIndex
                    + ", "
                    + colIndex
                    + "), cellValue="
                    + cell.getValue()
                    + '}';
        }
    }
}
