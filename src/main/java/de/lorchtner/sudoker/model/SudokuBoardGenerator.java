package de.lorchtner.sudoker.model;

import java.util.Objects;

import de.sfuhrm.sudoku.Creator;
import de.sfuhrm.sudoku.GameSchemas;
import de.sfuhrm.sudoku.Riddle;
import de.sfuhrm.sudoku.Solver;

public final class SudokuBoardGenerator {
    public static SudokuBoard generate() {
        return generate(Difficulty.MEDIUM);
    }

    public static SudokuBoard generate(Difficulty difficulty) {
        final var clearCount = fieldClearCount(Objects.requireNonNull(difficulty));
        final var matrix = Creator.createFull(GameSchemas.SCHEMA_9X9);

        Riddle riddle;
        int solutionCount;
        do {
            riddle = Creator.createRiddle(matrix, clearCount);
            solutionCount = new Solver(riddle).solve().size();
        } while (solutionCount != 1);

        return new SudokuBoard(riddle);
    }

    private static int fieldClearCount(Difficulty difficulty) {
        return switch (difficulty) {
			case LAME -> Creator.RIDDLE_9X9_EMPTY_FIELDS_VERY_EASY;
			case EASY -> Creator.RIDDLE_9X9_EMPTY_FIELDS_EASY;
			case MEDIUM -> Creator.RIDDLE_9X9_EMPTY_FIELDS_MEDIUM;
			case HARD -> Creator.RIDDLE_9X9_EMPTY_FIELDS_HARD;		
            case INSANE -> Creator.RIDDLE_9X9_EMPTY_FIELDS_VERY_HARD;
        };
    }
}
